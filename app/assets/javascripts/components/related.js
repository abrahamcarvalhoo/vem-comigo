$(function(){
  var relatedList = $('.related__list').swiper({
    speed: 500,
    touchRatio: 0.2,
    spaceBetween: 10,
    grabCursor: true,
    slidesPerView: 4,
    prevButton: '.related__nav--prev',
    nextButton: '.related__nav--next',
    breakpoints: {
      544: {
        loop: true,
        slidesPerView: 1.8,
        centeredSlides: true,
        slideToClickedSlide: true,
      },
      768: {
        loop: true,
        slidesPerView: 2.2,
        centeredSlides: true,
        slideToClickedSlide: true,
      },
      992: {
        loop: true,
        slidesPerView: 2.7,
        centeredSlides: true,
        slideToClickedSlide: true,
      },
      1200: {
        slidesPerView: 3,
        spaceBetween: 40,
      },
      2400: {
        slidesPerView: 4,
        spaceBetween: 40,
      },
      3000: {
        slidesPerView: 5,
      }
    }
  });
});
