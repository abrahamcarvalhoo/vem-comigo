$(function(){
  var roadmapList = $('.roadmap__list').swiper({
    speed: 500,
    loop: true,
    touchRatio: 0.2,
    spaceBetween: 10,
    grabCursor: true,
    slidesPerView: 3,
    centeredSlides: true,
    slideToClickedSlide: true,
    prevButton: '.roadmap__nav--prev',
    nextButton: '.roadmap__nav--next',
    breakpoints: {
      544: {
        slidesPerView: 1.7,
      },
      768: {
        slidesPerView: 2,
      },
      992: {
        slidesPerView: 2.5,
      },
      2400: {
        slidesPerView: 3,
        spaceBetween: 40,
      },
      3000: {
        slidesPerView: 5,
      }
    }
  });
});
