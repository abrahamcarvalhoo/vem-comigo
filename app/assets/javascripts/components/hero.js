$(function(){
  var agendaList = $('.hero__list').swiper({
    speed: 700,
    touchRatio: 0.2,
    slidesPerView: 1,
    simulateTouch: false,
    centeredSlides: true,
  });

  // var agendaList = $('.hero__list').swiper({
  //   speed: 700,
  //   touchRatio: 0.2,
  //   slidesPerView: 1,
  //   spaceBetween: 10,
  //   grabCursor: true,
  //   centeredSlides: true,
  //   keyboardControl: true,
  //   slideToClickedSlide: true,
  //   prevButton: '.hero__nav--prev',
  //   nextButton: '.hero__nav--next',
  //   breakpoints: {
  //     400: {
  //       slidesPerView: 2.4,
  //     },
  //     544: {
  //       slidesPerView: 1.7,
  //     },
  //     768: {
  //       slidesPerView: 1.5,
  //     },
  //     992: {
  //       slidesPerView: 1.6,
  //     },
  //     3000: {
  //       slidesPerView: 1,
  //     }
  //   }
  // });
});
