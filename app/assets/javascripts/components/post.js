$(function(){
  var lastScrollTop = 0;
  var share = $('.share');
  var totalHeight = $('.head').outerHeight() + $('.hero').outerHeight();

  if ($(window).width() >= 992) {
    $(window).scroll(function() {
      if ($(this).scrollTop() >= totalHeight) {
        share.addClass('is-active');
      } else {
        share.removeClass('is-active');
      }
    });
  } else {
    $(window).scroll(function() {
      var st = $(this).scrollTop();
      if (st > lastScrollTop) {
        share.addClass('is-active');
      } else {
        share.removeClass('is-active');
      }
      lastScrollTop = st;
    });
  }

  $('.comments__list-item__reply').click(function(event) {
    event.preventDefault();
    var id = $(this).attr('data-id');
    $('.comments__form #parent').val(id);
    $('.comments__list-item__content').removeClass('is-active');
    $(this).parent().addClass('is-active');
    $('html, body').animate({scrollTop: $('#commentform').offset().top - $('.header').outerHeight()}, 750);
  });

  $('html').on('click', '.post__navigation-item', function(event) {
    event.preventDefault();
    var target = $(this).attr('href');
    $('html, body').animate({ scrollTop: $(target).offset().top - $('.header').outerHeight() - 20}, 750);
  });

  if ($('.post__description h2').length) {
    $('.post__description h2').each(function(i, val) {
      var title = $(this).text();
      $(this).attr('id', slug(title));
      $('.post__navigation').append('<li><a class="post__navigation-item" href="#'+slug(title)+'">'+title+'</a></li>');
    });
  } else {
    $('.post__subtitle').remove();
    $('.post__navigation').remove();
  }
});
