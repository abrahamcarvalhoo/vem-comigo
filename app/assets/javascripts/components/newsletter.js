$(function(){
  $('.newsletter__form').validate({
    messages: {
      nome: 'Por favor insira seu nome.',
      email: 'Por favor insira um e-mail válido.',
    },
    submitHandler: function(form, event) {
      event.preventDefault();
      var params = $(form).serializeObject();

      $.ajax({
        url: '/wp-admin/admin-ajax.php?action=newsletter_form',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(params),
        contentType: 'application/json; charset=utf-8',
      }).done(function(data) {
        if (data.result == "success") {
          $('#alert').text('Inscrito com sucesso :)');
          $('#alert').addClass('is-active').addClass('is-success');
          setTimeout(function(){
            $('#alert').removeClass('is-active').removeClass('is-success');
          }, 5000);
        } else {
          $('#alert').text('Ops! algo deu errado :(');
          $('#alert').addClass('is-active').addClass('is-error');
          setTimeout(function(){
            $('#alert').removeClass('is-active').removeClass('is-error');
          }, 5000);
        }
      }).fail(function() {
        $('#alert').text('Ops! algo deu errado :(');
        $('#alert').addClass('is-active').addClass('is-error');
        setTimeout(function(){
          $('#alert').removeClass('is-active').removeClass('is-error');
        }, 5000);
      }).always(function() {
        $('.newsletter__form input').val('');
        $('.newsletter__form select').val('');
        $('.newsletter__form textarea').val('');
      });
    }
  });

  $('.social__form').validate({
    messages: {
      email: 'Por favor insira um e-mail válido.',
    },
    submitHandler: function(form, event) {
      event.preventDefault();
      var params = $(form).serializeObject();

      $.ajax({
        url: '/wp-admin/admin-ajax.php?action=newsletter_form',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(params),
        contentType: 'application/json; charset=utf-8',
      }).done(function(data) {
        if (data.result == "success") {
          $('#alert').text('Inscrito com sucesso :)');
          $('#alert').addClass('is-active').addClass('is-success');
          setTimeout(function(){
            $('#alert').removeClass('is-active').removeClass('is-success');
          }, 5000);
        } else {
          $('#alert').text('Ops! algo deu errado :(');
          $('#alert').addClass('is-active').addClass('is-error');
          setTimeout(function(){
            $('#alert').removeClass('is-active').removeClass('is-error');
          }, 5000);
        }
      }).fail(function() {
        $('#alert').text('Ops! algo deu errado :(');
        $('#alert').addClass('is-active').addClass('is-error');
        setTimeout(function(){
          $('#alert').removeClass('is-active').removeClass('is-error');
        }, 5000);
      }).always(function() {
        $('.social__form input').val('');
        $('.social__form select').val('');
        $('.social__form textarea').val('');
      });
    }
  });
});
