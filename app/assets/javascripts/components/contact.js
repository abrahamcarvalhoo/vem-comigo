$(function(){
  $('.contact__form').validate({
    errorPlacement: function(error, element) {},
    submitHandler: function(form, event) {
      event.preventDefault();
      var params = $(form).serializeObject();

      $.ajax({
        url: '/wp-admin/admin-ajax.php?action=contact_form',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(params),
        contentType: 'application/json; charset=utf-8',
      }).done(function(data) {
        if (data.result == "success") {
          $('#alert').text('Enviado com sucesso :)');
          $('#alert').addClass('is-active').addClass('is-success');
          setTimeout(function(){
            $('#alert').removeClass('is-active').removeClass('is-success');
          }, 5000);
        } else {
          $('#alert').text('Ops! algo deu errado :(');
          $('#alert').addClass('is-active').addClass('is-error');
          setTimeout(function(){
            $('#alert').removeClass('is-active').removeClass('is-error');
          }, 5000);
        }
      }).fail(function() {
        $('#alert').text('Ops! algo deu errado :(');
        $('#alert').addClass('is-active').addClass('is-error');
        setTimeout(function(){
          $('#alert').removeClass('is-active').removeClass('is-error');
        }, 5000);
      }).always(function() {
        $('.contact__form input').val('');
        $('.contact__form select').val('');
        $('.contact__form textarea').val('');
      });
    }
  });
});
