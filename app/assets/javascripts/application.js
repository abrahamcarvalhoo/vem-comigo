//=include components/hero.js
//=include components/post.js
//=include components/header.js
//=include components/photos.js
//=include components/roadmap.js
//=include components/related.js
//=include components/contact.js
//=include components/newsletter.js

// var wow = new WOW({mobile: false}).init();

// Validate
$('.header__search').validate({
  errorPlacement: function(error, element) {}
});

$('.footer__search').validate({
  errorPlacement: function(error, element) {}
});

$('.search__form').validate({
  errorPlacement: function(error, element) {}
});

$('.comments__form').validate({
  errorPlacement: function(error, element) {}
});

$('.notfound__form').validate({
  errorPlacement: function(error, element) {}
});

// Masks
$('input.day').mask('00');
$('input.time').mask('00:00');
$('input.cep').mask('00000-000');
$('input.date').mask('00/00/0000');
$('input.number').mask('00000000');
$('input.tel').mask(SPMaskBehavior, spOptions);
$('input.cpf').mask('000.000.000-00', {reverse: true});
$('input.cnpj').mask('00.000.000/0000-00', {reverse: true});
