$(function(){
  if ($('.photos__list').length) {
    $.ajax({
      url: '/instagram.php?count=10',
      type: 'GET',
      async: true,
      dataType: 'jsonp',
      success: function(data) {
        var photos = '';

        $.each(data, function(i, item) {
          photos += '<a class="photos__list-item swiper-slide" href="'+this.link+'" target="_blank">';
          photos += '<picture>';
          photos += '<source media="(max-width: 400px)" srcset="'+this.url[1].src+'">';
          photos += '<img class="photos__list-item__image" src="'+this.url[3].src+'" alt="instagram">';
          photos += '</picture>';
          photos += '</a>';
        });

        $('.photos__list .swiper-wrapper').html(photos);

        var photosList = $('.photos__list').swiper({
          speed: 500,
          touchRatio: 0.5,
          spaceBetween: 10,
          slidesPerView: 10,
          breakpoints: {
            400: {
              slidesPerView: 3,
              slidesPerColumn: 2,
            },
            544: {
              loop: true,
              slidesPerView: 2,
              slidesPerColumn: 2,
              centeredSlides: true,
            },
            768: {
              slidesPerView: 3,
              slidesPerColumn: 2,
            },
            992: {
              loop: true,
              slidesPerView: 4,
              slidesPerColumn: 2,
              centeredSlides: true,
            },
            1200: {
              slidesPerView: 6,
            },
            1400: {
              slidesPerView: 7,
            },
            1600: {
              slidesPerView: 8,
            },
            3000: {
              slidesPerView: 10,
            },
          }
        });
      }
    });
  }
});

  // var photosList = $('.photos__list').swiper({
  //   speed: 500,
  //   touchRatio: 0.5,
  //   spaceBetween: 10,
  //   slidesPerView: 10,
  //   breakpoints: {
  //     400: {
  //       slidesPerView: 3,
  //       slidesPerColumn: 2,
  //     },
  //     544: {
  //       loop: true,
  //       slidesPerView: 2,
  //       slidesPerColumn: 2,
  //       centeredSlides: true,
  //     },
  //     768: {
  //       slidesPerView: 3,
  //       slidesPerColumn: 2,
  //     },
  //     992: {
  //       loop: true,
  //       slidesPerView: 4,
  //       slidesPerColumn: 2,
  //       centeredSlides: true,
  //     },
  //     1200: {
  //       slidesPerView: 6,
  //     },
  //     1400: {
  //       slidesPerView: 7,
  //     },
  //     1600: {
  //       slidesPerView: 8,
  //     },
  //     3000: {
  //       slidesPerView: 10,
  //     },
  //   }
  // });
