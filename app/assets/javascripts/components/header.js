$(function(){
  var head = $('.head');
  var header = $('.header');
  var scrolltop = $('.scrolltop');
  var headAnimation = $('.head__container');

  if ($(window).width() >= 992) {
    $(window).scroll(function() {
      if ($(this).scrollTop() > head.outerHeight()) {
        header.addClass('is-active');
      } else {
        header.removeClass('is-active');

        var scale = 1.0 + $(window).scrollTop() / 400 * 1;
        var opacity = 1.0 - $(window).scrollTop() / 150 * 1;

        headAnimation.css({
          opacity: opacity,
          transform: 'scale('+scale+')',
        });
      }
    });
  } else {
    $('.overlay').click(function() {
      $(this).removeClass('is-active');
      $('.header__search').removeClass('is-active');
    });

    $('.header__opensearch').click(function() {
      $('.overlay').addClass('is-active');
      $('.header__search').addClass('is-active');
    });

    $('.header__search-back').click(function() {
      $('.header__search').removeClass('is-active');
      $('.overlay').removeClass('is-active');
    });

    $('.header__navigation').click(function() {
      $(this).toggleClass('is-active');
      $('.header__menu').toggleClass('is-active');
      $('.header__submenu').fadeOut();
    });

    $('span.header__menu-item').click(function() {
      $('.header__submenu').hide();
      $(this).next().fadeToggle('fast');
    });
  }

  scrolltop.click(function() {
    $('html, body').animate({scrollTop: 0}, 750);
  });

  $(window).scroll(function() {
    if ($(this).scrollTop() > head.outerHeight()) {
      scrolltop.addClass('is-active');
    } else {
      scrolltop.removeClass('is-active');
    }
  });
});
